# Zephyr: VBUS Detection

## What is `VBUS`?
![](vbus.png)

## `nRF52833` Product Specification
![](vbus_docs.png)

## `VBUS` Detection
![](vbus_detection.png)

## Why is this important?
* Safety!  Shock risk
* USB charging / power provides a physical connection to a power source.
* IEC60601 has very different safety testing requirements for "wall" powered vs. battery-powered devices.

## Firmware Implementation
```C
#include <nrfx_power.h> // NOTE: This is not a Zephyr library!!  It is a Nordic NRFX library.

bool usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
if (usbregstatus) {
    // VBUS detected
} else {
    // VBUS not detected
}
```
* If you have a battery-powered device that cannot be used while charging, could change to "charging" state that would block operation.

## Drawbacks
* Not ideal; this is now chip-specific code.
* This breaks the Zephyr abstraction layer; now firmware code is hardware-specific. :(
* But... Zephyr `usbc_vbus` API is not implemented for the `nRF52833`.

## How to Test
You can use the second USB port (`nRF USB`) on the nRF52833 DK to test this.  The second USB port is connected directly to the `VBUS` pin on the nRF52833.

![](image.png)

## Resources
* [Zephyr: USB-C VBUS](https://docs.zephyrproject.org/latest/hardware/peripherals/usbc_vbus.html)